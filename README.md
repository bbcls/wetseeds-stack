# 🎄 WetSeeds Stack
Ce projet contient un fichier `docker-compose.yml` qui permet de démarrer les conteneurs pour l'API et le Frontend Vue.js en utilisant Docker Compose.

## Instructions

### Prérequis

Assurez-vous d'avoir Docker et Docker Compose installés sur votre machine.

### Étapes pour démarrer les conteneurs avec Docker Compose

```shell
git clone https://gitlab.com/bbcls/wetseeds-stack

cd wetseeds-stack

docker-compose up -d
```

### Étapes pour démarrer les conteneurs individuellement

#### API

```shell
docker run -d -p 7056:80 registry.gitlab.com/bbcls/wetseeds-api:latest
```

#### Front Vue

```shell
docker run -d -p 80:80 registry.gitlab.com/bbcls/wsg-vue/main:latest
```
